package com.nsa.grp9pdl.login;

import com.nsa.grp9pdl.User.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepoJPA extends JpaRepository<User, Integer> {
    Optional<User> findByEmail(String userName);
}
