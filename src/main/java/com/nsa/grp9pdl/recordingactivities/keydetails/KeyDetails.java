package com.nsa.grp9pdl.recordingactivities.keydetails;

import com.nsa.grp9pdl.recordingactivities.activity.Activity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "keydetails")
public class KeyDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="keydetailsID")
    private Integer KeyDetailsID;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="activityID", referencedColumnName = "activityID")
    private Activity activityID;
    private String description;
    private String role;
    @Column(name="othersinvolved")
    private String othersInvolved;
    private String impact;
    @Column(name="learningtech")
    private String learningTech;

}
