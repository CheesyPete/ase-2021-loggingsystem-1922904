package com.nsa.grp9pdl.recordingactivities.keydetails;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KeyDetailsForm {
    private String description;
    private String role;
    private String othersInvolved;
    private String impact;
    private String learningTech;
    private String thoughtCloud;
}
