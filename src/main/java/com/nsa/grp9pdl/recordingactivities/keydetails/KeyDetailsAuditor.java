package com.nsa.grp9pdl.recordingactivities.keydetails;

import com.nsa.grp9pdl.recordingactivities.activity.Activity;
import org.springframework.stereotype.Component;

@Component
public interface KeyDetailsAuditor {
    void saveKeyDetails(KeyDetailsForm keyDetailsForm, Integer aID);
    void updateKeyDetails(KeyDetailsForm keyDetails, Integer aID);
    KeyDetails findKeyDetailsByActivityID(Activity activity);
}
