package com.nsa.grp9pdl.recordingactivities.keydetails;

import com.nsa.grp9pdl.recordingactivities.activity.Activity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KeyDetailsRepo extends JpaRepository<KeyDetails, Integer> {
    KeyDetails findByActivityID(Activity activity);
}
