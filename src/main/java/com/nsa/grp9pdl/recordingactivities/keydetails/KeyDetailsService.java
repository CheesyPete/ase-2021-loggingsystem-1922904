package com.nsa.grp9pdl.recordingactivities.keydetails;

import com.nsa.grp9pdl.recordingactivities.activity.Activity;
import com.nsa.grp9pdl.recordingactivities.activity.ActivityService;
import com.nsa.grp9pdl.security.AESEncrypter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Configuration
public class KeyDetailsService implements KeyDetailsAuditor {
    private final KeyDetailsRepo keyDetailsRepo;
    private final ActivityService activityService;
    @Value("${encryption.key}")
    private String secretKey;

    public KeyDetailsService(KeyDetailsRepo kdR, ActivityService aS) {
        keyDetailsRepo = kdR;
        activityService = aS;
    }

    private KeyDetails createNewKeyDetails(KeyDetailsForm kdf, Integer id) {

        KeyDetails keyDetails = new KeyDetails();
        Activity actID = activityService.getActivity(id);
        keyDetails.setActivityID(actID);
        String kdDes = kdf.getDescription();
        String encryptedDescription = AESEncrypter.encrypt(kdDes, secretKey);
        keyDetails.setDescription(encryptedDescription);
        keyDetails.setRole(kdf.getRole());

        String kdOthers = kdf.getOthersInvolved();
        String encryptedOthers = AESEncrypter.encrypt(kdOthers, secretKey);
        keyDetails.setOthersInvolved(encryptedOthers);
        keyDetails.setImpact(kdf.getImpact());
        keyDetails.setLearningTech(kdf.getLearningTech());
        return keyDetails;
    }

    @Override
    public void saveKeyDetails(KeyDetailsForm keyDetailsForm, Integer activity) {
        KeyDetails newKD = createNewKeyDetails(keyDetailsForm, activity);
        keyDetailsRepo.save(newKD);
    }
    @Override
    public void updateKeyDetails(KeyDetailsForm newR, Integer id) {
        Activity activity = activityService.getActivity(id);
        KeyDetails kd = keyDetailsRepo.findByActivityID(activity);
        kd.setActivityID(activity);
        kd.setRole(newR.getRole());
        kd.setOthersInvolved(newR.getOthersInvolved());
        kd.setImpact(newR.getImpact());
        kd.setLearningTech(newR.getLearningTech());
        keyDetailsRepo.save(kd);
    }

    public KeyDetailsForm getFormBasedOnKeyDetails(Integer id) {
        Activity activity = activityService.getActivity(id);
        KeyDetails kd = keyDetailsRepo.findByActivityID(activity);
        KeyDetailsForm kdF = new KeyDetailsForm();
        if (kd != null) {
            kdF.setRole(kd.getRole());
            kdF.setOthersInvolved(kd.getOthersInvolved());
            kdF.setImpact(kd.getImpact());
            kdF.setLearningTech(kd.getLearningTech());
        }
        return kdF;
    }


    @Override
    public KeyDetails findKeyDetailsByActivityID(Activity id) {
        return keyDetailsRepo.findByActivityID(id);
    }


    public KeyDetails getKeyDetailsForActivity(int id) {
        Activity currentAct = activityService.getActivity(id);
        KeyDetails stuff = keyDetailsRepo.findByActivityID(currentAct);
        return stuff;
    }

}
