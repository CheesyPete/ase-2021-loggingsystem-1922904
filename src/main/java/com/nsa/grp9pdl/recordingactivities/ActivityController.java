package com.nsa.grp9pdl.recordingactivities;

import com.lowagie.text.DocumentException;
import com.nsa.grp9pdl.ToDo.SubmittedCheckboxes;
import com.nsa.grp9pdl.ToDo.ToDo;
import com.nsa.grp9pdl.ToDo.ToDoForm;
import com.nsa.grp9pdl.ToDo.ToDoService;
import com.nsa.grp9pdl.User.User;
import com.nsa.grp9pdl.User.UserRepositoryJPA;
import com.nsa.grp9pdl.dashboard.dashDataJSON;
import com.nsa.grp9pdl.exporting.ExportToPdf;
import com.nsa.grp9pdl.recordingactivities.activity.Activity;
import com.nsa.grp9pdl.recordingactivities.activity.ActivityRepositoryJPA;
import com.nsa.grp9pdl.recordingactivities.activity.ActivityService;
import com.nsa.grp9pdl.recordingactivities.grid.GridQuestionAnswerList;
import com.nsa.grp9pdl.recordingactivities.grid.gridanswers.GridAnswersForm;
import com.nsa.grp9pdl.recordingactivities.grid.gridanswers.GridAnswersService;
import com.nsa.grp9pdl.recordingactivities.grid.gridquestions.GridQuestions;
import com.nsa.grp9pdl.recordingactivities.grid.gridquestions.GridQuestionsService;
import com.nsa.grp9pdl.recordingactivities.keydetails.KeyDetails;
import com.nsa.grp9pdl.recordingactivities.keydetails.KeyDetailsForm;
import com.nsa.grp9pdl.recordingactivities.keydetails.KeyDetailsService;
import com.nsa.grp9pdl.recordingactivities.reflections.ReflectionForm;
import com.nsa.grp9pdl.recordingactivities.reflections.Reflections;
import com.nsa.grp9pdl.recordingactivities.reflections.ReflectionsService;
import com.nsa.grp9pdl.recordingactivities.tagging.TagDate;
import com.nsa.grp9pdl.recordingactivities.tagging.TagLink;
import com.nsa.grp9pdl.recordingactivities.tagging.TaggingForm;
import com.nsa.grp9pdl.recordingactivities.tagging.TaggingService;
import com.nsa.grp9pdl.recordingactivities.tagging.Tags;
import com.nsa.grp9pdl.recordingactivities.wordcloud.WordCloud;
import com.nsa.grp9pdl.recordingactivities.wordcloud.WordCloudForm;
import com.nsa.grp9pdl.recordingactivities.wordcloud.WordCloudService;
import com.nsa.grp9pdl.security.AESEncrypter;
import com.nsa.grp9pdl.security.CustomAuthenticationSuccessHandler;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.persistence.PostRemove;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Controller
public class ActivityController {
    @Autowired
    private GridAnswersService gridAnswersService;
    @Autowired
    private GridQuestionsService gridQuestionsService;
    @Autowired
    private ReflectionsService reflectionsService;
    @Autowired
    private ActivityService activityService;
    @Autowired
    private KeyDetailsService keyDetailsService;
    @Autowired
    private WordCloudService wordCloudService;
    @Autowired
    private UserRepositoryJPA userRepositoryJPA;
    @Autowired
    private TaggingService taggingService;
    private String d;
    private String oi;
    @Autowired
    private ToDoService toDoService;
    @Value("${encryption.key}")
    private String secretKey;
    private String lastGridQuestion;

    @GetMapping("/new-activity/keydetails/{id}")
    public String showForm(@PathVariable(name = "id", required = true) String id, Model model) {
        KeyDetailsForm keyDetailsForm = new KeyDetailsForm();
        model.addAttribute("kdForm", keyDetailsForm);
        model.addAttribute("aid", id);
        return "keydetails";
    }

    @PostMapping("/new-activity/keydetails/{id}")
    public String sendForm(@PathVariable(name = "id", required = true) String id, @ModelAttribute KeyDetailsForm kdf,
            Model model) {
        Integer i = Integer.parseInt(id);
        model.addAttribute(kdf);
        keyDetailsService.saveKeyDetails(kdf, i);
        return "redirect:/viewactivity/" + id;
    }

    @GetMapping("/new-activity/reflections/{id}")
    public String createNewReflection(@PathVariable(name = "id", required = true) String id, Model model) {
        ReflectionForm reflectionForm = new ReflectionForm();
        model.addAttribute("aid", id);
        model.addAttribute("reflectionForm", reflectionForm);
        return "new-reflection";
    }

    @PostMapping("/new-activity/reflections/{id}")
    public String storeNewReflection(@PathVariable(name = "id", required = true) String id,
            @ModelAttribute ReflectionForm reflectionForm, Model model) {
        Integer aID = Integer.valueOf(id);
        model.addAttribute(reflectionForm);
        Activity activity = activityService.getActivity(aID);
        model.addAttribute("activity", activity);
        reflectionsService.saveReflection(reflectionForm, aID);
        return "redirect:/viewactivity/" + aID;
    }

    @PostMapping("/edit-activity/keydetails/{id}")
    public String editKD(@PathVariable(name = "id", required = true) String id, @ModelAttribute KeyDetailsForm ekdf,
            Model model) {
        Integer aID = Integer.valueOf(id);
        keyDetailsService.updateKeyDetails(ekdf, aID);
        return "redirect:/viewactivity/" + aID;
    }

    @PostMapping("/edit-activity/reflections/{id}")
    public String editRef(@PathVariable(name = "id", required = true) String id, @ModelAttribute ReflectionForm eref) {
        Integer aID = Integer.valueOf(id);
        reflectionsService.updateReflection(eref, aID);
        return "redirect:/viewactivity/" + aID;
    }

    @PostMapping("/edit-activity/dimensions/{id}")
    public String edittags(@PathVariable(name = "id", required = true) String id, @ModelAttribute TaggingForm etags) {
        Integer aID = Integer.valueOf(id);
        taggingService.updateTags(etags, aID);
        return "redirect:/viewactivity/" + aID;
    }

    @GetMapping("/wordcloud/{id}")
    public String quickReflectionWordCloud(@PathVariable(name = "id", required = true) String id, Model model) {

        WordCloudForm newWordCloudForm = new WordCloudForm();
        model.addAttribute("wid", id);
        model.addAttribute("wordCloudForm", newWordCloudForm);
        return "word-cloud";
    }

    @PostMapping("/wordcloud/{id}")
    public String saveWordCloud(@ModelAttribute WordCloudForm newWordCloudForm,
            @PathVariable(name = "id", required = true) String id, Model model, BindingResult bindingResult) {
        Integer i = Integer.parseInt(id);
        wordCloudService.saveWordCloud(newWordCloudForm, i);

        if (bindingResult.hasErrors()) {
            return "redirect:/word-cloud";
        } else {
            return "redirect:/viewactivity/" + id;
        }
    }

    @GetMapping("/viewcloud/{id}")
    public String viewIndivClouds(@PathVariable(name = "id") String id, Model model) {
        Integer wcid = Integer.valueOf(id);
        WordCloud wordCloud = wordCloudService.viewCloud(wcid);
        JSONArray array = new JSONArray();
        Map<String, Object> stuff = wordCloud.getWordCloudData();
        for (Map.Entry<String, Object> entry : stuff.entrySet()) {
            JSONObject record = new JSONObject();
            record.put("x", entry.getKey());
            record.put("value", entry.getValue());
            array.put(record);
        }
        model.addAttribute("wc", array);
        return "displaycloud";
    }

    @GetMapping("/new-activity/dimensions/{id}")
    public String recordedTags(@PathVariable(name = "id", required = true) String id, Model model) {
        model.addAttribute("TiD", id);
        TaggingForm recordedtags = new TaggingForm();
        model.addAttribute("taggingForm", recordedtags);
        return "tagging-activities";
    }

    @PostMapping("/new-activity/dimensions/{id}")
    public String submitTag(@PathVariable(name = "id", required = true) String id,
            @ModelAttribute TaggingForm recordedtags, Model model, BindingResult bindings) {

        if (bindings.hasErrors()) {
            return "redirect:/dashboard";
        } else {

            Integer TiD = Integer.parseInt(id);
            model.addAttribute("TiD", TiD);

            taggingService.saveNewTag(recordedtags, TiD);
            return "redirect:/viewactivity/" + TiD;
        }
    }

    @GetMapping("/grid-reflection/{id}")
    public String showGridReflection(@PathVariable(name = "id", required = true) String id, Model model) {
        GridAnswersForm gridData = new GridAnswersForm();

        List<GridQuestions> questionList = gridQuestionsService.findAll();
        Collections.shuffle(questionList);

        model.addAttribute("savedquesion", lastGridQuestion);
        lastGridQuestion = null;
        model.addAttribute("aid", id);
        model.addAttribute("gridList", questionList);
        model.addAttribute("gForm", gridData);
        return "grid-reflection";
    }

    @PostMapping("/grid-reflection/{aID}/{gID}")
    public String showGridReflection(@ModelAttribute GridAnswersForm gridAnswersForm,
            @PathVariable(name = "aID", required = true) String aID, Model model,
            @PathVariable(name = "gID", required = true) String gID, BindingResult bindings) {
        GridAnswersForm gridData = new GridAnswersForm();
        Integer aintID = Integer.valueOf(aID);
        Integer gintID = Integer.valueOf(gID);

        gridAnswersService.saveGridAnswers(gridAnswersForm, aintID, gintID);
        List<GridQuestions> questionList = gridQuestionsService.findAll();
        Collections.shuffle(questionList);

        lastGridQuestion = gridQuestionsService.getGridQuestions(gintID).getQuestion();
        return "redirect:/grid-reflection/" + aID;
    }

    @GetMapping("/dashboard")
    public String showDashboard(Model model) {
        Integer userID = CustomAuthenticationSuccessHandler.currentUserDetails().getUserID();
        String userName = CustomAuthenticationSuccessHandler.currentUserDetails().getUsername();

        User aUsers = userRepositoryJPA.findUserById(userID);
        List<TagDate> userTags = taggingService.findTagsForUser(aUsers);
        List<ToDo> toDos = toDoService.findAllByUserIDAndCompleted(aUsers, false);
        Map<Tags, Integer> userTagTotals = taggingService.findTotalTags(userTags);
        int activityTotal = taggingService.findTotalActivitiesForUser(aUsers);
        int tagTotal = taggingService.findTotalTagsForUser(userTags);

        Map<Tags, List<Integer>> dailyTagIncrease = taggingService.getTagIncreaseByDay(userTags, 14);
        long millisDifference = Math.abs(new Date().getTime() - taggingService.getLastDate(userTags).getTime());
        long daysDifference = TimeUnit.DAYS.convert(millisDifference, TimeUnit.MILLISECONDS);


        List<String> tagNames = new dashDataJSON().getLineTagNames(dailyTagIncrease);
        List<Object> aLineData = new dashDataJSON().lineGraphJSON(dailyTagIncrease, taggingService.findPastDates(14));
        List<Object> aDonut1Data = new dashDataJSON().donutChart1JSON(userTagTotals);
        List<Object> aDonut2Data = new dashDataJSON().donutChart2JSON(userTagTotals);
        List<Object> aDonut3Data = new dashDataJSON().donutChart3JSON(userTagTotals);

        SubmittedCheckboxes submittedCheckboxes = new SubmittedCheckboxes();
        model.addAttribute("toDos", toDos);
        model.addAttribute("aLineData", aLineData);
        model.addAttribute("donutData1", aDonut1Data);
        model.addAttribute("donutData2", aDonut2Data);
        model.addAttribute("donutData3", aDonut3Data);
        model.addAttribute("userName", userName);

        model.addAttribute("daysSince", daysDifference);
        model.addAttribute("aTagData", tagNames);
        model.addAttribute("activityTotal", activityTotal);
        model.addAttribute("tagTotal", tagTotal);
        model.addAttribute("sc", submittedCheckboxes);

        return "dashboard";
    }

    @GetMapping("/reflection-list")
    public String showReflectionList(Model model) {
        List<Activity> activities = activityService.getAllActivities();
        List<Integer> percentages = activityService.getActivityPercentage(activities);
        model.addAttribute("activities", activities);
        model.addAttribute("percentages", percentages);
        return "reflection-list";
    }

    @GetMapping("/viewactivity/{id}")
    public String showIndivActivity(@PathVariable(name = "id", required = true) String id, Model model) {
        Integer activID = Integer.valueOf(id);
        boolean showExport = false;
        KeyDetails kd = keyDetailsService.getKeyDetailsForActivity(activID);
        String decryptedDescription;
        String decryptedOthers;
        GridQuestionAnswerList questionAnswerList = new GridQuestionAnswerList();

        questionAnswerList.setAnswerList(gridAnswersService.findByActivityID(activID));
        List<GridQuestions> allQuestions = gridQuestionsService.findAll();
        questionAnswerList.setQuestionList(allQuestions);
        questionAnswerList.build();

        KeyDetailsForm kdForm = keyDetailsService.getFormBasedOnKeyDetails(activID);
        ReflectionForm rForm = reflectionsService.getFormBasedOnReflections(activID);
        TaggingForm tForm = taggingService.getFormBasedOnTagging(activID);
        JSONArray array = wordCloudService.viewFullCloud(activID);
        Boolean isWordCloud = true;
        if (kdForm != null) {
            model.addAttribute("kdForm", kdForm);
        }
        if (rForm != null) {
            model.addAttribute("rForm", rForm);
        }
        if (tForm != null) {
            model.addAttribute("tForm", tForm);
        }
        if (array != null && array.length() > 0) {
            isWordCloud = false;
        }

        if (kd != null) {
            String kdDesDecrypt = kd.getDescription();
            decryptedDescription = AESEncrypter.decrypt(kdDesDecrypt, secretKey);
            d = decryptedDescription;
        } else {
            decryptedDescription = "";

        }
        if (kd != null) {
            String kdOthersDecrypt = kd.getOthersInvolved();
            decryptedOthers = AESEncrypter.decrypt(kdOthersDecrypt, secretKey);
            oi = decryptedOthers;
        } else {
            decryptedOthers = "";

        }

        Reflections rf = reflectionsService.findReflections(activID);
        List<TagLink> tg = taggingService.getAllTags(activID);
        List<Tags> tgg = taggingService.getAllTagDetails(tg);
        if (rf != null && kd != null && !tgg.isEmpty()) {
            showExport = true;
        }
        model.addAttribute("isWordCloud", isWordCloud);
        model.addAttribute("wc", array);
        model.addAttribute("gr", questionAnswerList.getGridQuestionAnswerPairList());
        model.addAttribute("keydetails", kd);
        model.addAttribute("decryptedDescription", decryptedDescription);
        model.addAttribute("decryptedOthers", decryptedOthers);
        model.addAttribute("reflections", rf);
        model.addAttribute("tags", tgg);
        model.addAttribute("aID", activID);
        model.addAttribute("showExport", showExport);
        return "viewactivity";
    }

    @GetMapping("add-todo")
    public String ADDtodo(Model model) {
        ToDoForm toDoForm = new ToDoForm();

        model.addAttribute("toDoForm", toDoForm);

        return "ToDo";
    }

    @PostMapping("/add-todo")
    public String newToDo(ToDoForm toDoForm) {
        Integer userID = CustomAuthenticationSuccessHandler.currentUserDetails().getUserID();
        toDoService.createNewToDo(toDoForm.getName(), userID);
        return "redirect:/dashboard";

    }
    @PostMapping("/remove-todo")
    public String removeToDo(@ModelAttribute SubmittedCheckboxes submittedCheckboxes) {
        toDoService.modifyToDo(submittedCheckboxes);
        return "redirect:/dashboard";
    }

    @GetMapping("/exportDashboard")
    public String showExport(HttpServletResponse response) throws Exception {
        response.setContentType("application/pdf");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=" + "DashboardData" + currentDateTime + ".pdf";
        response.setHeader(headerKey, headerValue);
        ExportToPdf exporter = new ExportToPdf(keyDetailsService, reflectionsService, taggingService,
                userRepositoryJPA);
        exporter.export(response);
        return "redirect:/dashboard";
    }

    @GetMapping("/exportActivity/{id}")
    public String showActivityExport(@PathVariable(name = "id") String id, HttpServletResponse response)
            throws DocumentException, IOException {
        Integer aID = Integer.valueOf(id);
        Activity activity = activityService.getActivity(aID);
        response.setContentType("application/pdf");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        String currentDateTime = dateFormatter.format(new Date());
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=" + activity.getTitle() + currentDateTime + ".pdf";
        response.setHeader(headerKey, headerValue);
        ExportToPdf exporter = new ExportToPdf(keyDetailsService, reflectionsService, taggingService,
                userRepositoryJPA);
        exporter.export(response, activity, d, oi);
        return "/dashboard";
    }
}
