package com.nsa.grp9pdl.recordingactivities.reflections;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReflectionForm {
    @NotEmpty
    @NotNull
    @Size(max = 255)
    private String title;

    @NotEmpty
    @NotNull
    @Size(max = 255)
    private String approach;

    @NotEmpty
    @NotNull
    @Size(max = 255)
    private String happenings;

    @NotEmpty
    @NotNull
    @Size(max = 255)
    private String evaluation;

    @NotEmpty
    @NotNull
    @Size(max = 255)
    private boolean needEvidence;

    private String evidence;

    @NotEmpty
    @NotNull
    @Size(max = 255)
    private String ebi;

    @NotEmpty
    @NotNull
    @Size(max = 255)
    private String learningPoints;
}
