package com.nsa.grp9pdl.recordingactivities.reflections;

import com.nsa.grp9pdl.recordingactivities.activity.Activity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReflectionsRepo extends JpaRepository<Reflections, Integer> {
    Reflections findByActivityID(Activity activity);

}
