package com.nsa.grp9pdl.recordingactivities.reflections;

import com.nsa.grp9pdl.recordingactivities.activity.Activity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.persistence.Column;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "reflections")
public class Reflections {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "reflectionsID")
    private int reflectionID;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "activityID", referencedColumnName = "activityID")
    private Activity activityID;
    private String title;
    private String approach;
    private String happenings;
    private String evaluation;
    @Column(name = "needevidence")
    private boolean needEvidence;
    private String evidence;
    private String ebi;
    @Column(name = "learningpoints")
    private String learningpoints;
}
