package com.nsa.grp9pdl.recordingactivities.reflections;

import com.nsa.grp9pdl.recordingactivities.activity.Activity;
import com.nsa.grp9pdl.recordingactivities.activity.ActivityService;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ReflectionsService implements ReflectionsAuditor {

    private final ReflectionsRepo reflectionsRepo;
    private final ActivityService activityService;

    public ReflectionsService(ReflectionsRepo rRepo, ActivityService aS) {
        reflectionsRepo = rRepo;
        activityService = aS;
    }

    private Reflections createNewReflection(ReflectionForm rf, Integer id) {
        Reflections newR = new Reflections();
        Activity actID = activityService.getActivity(id);

        newR.setActivityID(actID);
        newR.setTitle(rf.getTitle());
        newR.setApproach(rf.getApproach());
        newR.setHappenings(rf.getHappenings());
        newR.setEvaluation(rf.getTitle());
        boolean nEvi = rf.isNeedEvidence();
        newR.setNeedEvidence(nEvi);

        if (nEvi) {
            newR.setEvidence(rf.getEvidence());
        } else {
            newR.setEvidence("N/A");
        }

        newR.setEbi(rf.getEbi());
        newR.setLearningpoints(rf.getLearningPoints());

        return newR;
    }

    @Override
    public void saveReflection(ReflectionForm nForm, Integer id) {
        Reflections newR = createNewReflection(nForm, id);

        reflectionsRepo.save(newR);
    }

    @Override
    public void updateReflection(ReflectionForm newR, Integer id) {
        Activity activity = activityService.getActivity(id);
        Reflections ref = reflectionsRepo.findByActivityID(activity);
        ref.setActivityID(activity);
        ref.setTitle(newR.getTitle());
        ref.setApproach(newR.getApproach());
        ref.setHappenings(newR.getHappenings());
        ref.setEvaluation(newR.getEvaluation());
        ref.setNeedEvidence(newR.isNeedEvidence());
        ref.setEvidence(newR.getEvidence());
        ref.setEbi(newR.getEbi());
        ref.setLearningpoints(newR.getLearningPoints());
        reflectionsRepo.save(ref);
    }


        @Override
    public Optional<Reflections> findReflectionsById(Integer id) {
        Optional<Reflections> allReflections = reflectionsRepo.findById(id);

        return allReflections;
    }
    public Reflections findReflections(int id){
        Activity currentAct = activityService.getActivity(id);
        Reflections stuff = reflectionsRepo.findByActivityID(currentAct);
        return stuff;
    }
    public ReflectionForm getFormBasedOnReflections(Integer id) {
        Activity activity = activityService.getActivity(id);
        Reflections ref = reflectionsRepo.findByActivityID(activity);
        ReflectionForm refF = new ReflectionForm();
        if (ref != null) {
            refF.setApproach(ref.getApproach());
            refF.setNeedEvidence(ref.isNeedEvidence());
            if (ref.isNeedEvidence()) {
                refF.setEvidence(ref.getEvidence());
            }
            refF.setEbi(ref.getEbi());
            refF.setHappenings(ref.getHappenings());
            refF.setEvaluation(ref.getEvaluation());
            refF.setLearningPoints(ref.getLearningpoints());
        }
        return refF;
    }


}
