package com.nsa.grp9pdl.recordingactivities.wordcloud;

import com.nsa.grp9pdl.recordingactivities.activity.Activity;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.Map;

@Data
@Entity
@Table(name = "wordcloud")
public class WordCloud {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "wordcloudid")
    private Integer wordCloudID;
    @OneToOne
    @JoinColumn(name = "activityID", referencedColumnName = "activityID")
    private Activity activityID;
    @Convert(converter = JpaConverterJson.class)
    @Column(name = "wordclouddata")
    private Map<String, Object> wordCloudData;
    
}
