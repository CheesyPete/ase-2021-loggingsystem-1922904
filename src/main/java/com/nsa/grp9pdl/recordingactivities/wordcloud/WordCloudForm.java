package com.nsa.grp9pdl.recordingactivities.wordcloud;

import lombok.Data;

@Data
public class WordCloudForm {
    private String wordclouddata;
}
