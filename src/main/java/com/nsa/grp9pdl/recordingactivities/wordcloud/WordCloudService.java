package com.nsa.grp9pdl.recordingactivities.wordcloud;

import com.nsa.grp9pdl.recordingactivities.activity.Activity;
import com.nsa.grp9pdl.recordingactivities.activity.ActivityService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class WordCloudService {
    private final WordCloudRepository wordCloudRepo;
    private final ActivityService activityService;

    public WordCloudService(WordCloudRepository wRepo, ActivityService aS) {
        wordCloudRepo = wRepo;
        activityService = aS;
    }
    private Map<String, Object> StringToMap(WordCloudForm wordCloudForm) {
        Map<String, Object> wordCloudDataToMap = new HashMap<>();
        String wordCloudData = wordCloudForm.getWordclouddata();
        String[] test = wordCloudData.split(";");
        for(String s:test) {
            String[] test2 = s.split(",");
            wordCloudDataToMap.put(test2[0], test2[1]);
        }
        return wordCloudDataToMap;
    }
    private WordCloud createNewWordCloud(WordCloudForm wordCloudForm, Integer id) {
        WordCloud newWC = new WordCloud();
        Activity activity = activityService.getActivity(id);
        newWC.setActivityID(activity);
        newWC.setWordCloudData(StringToMap(wordCloudForm));
        return newWC;
    }

    public void saveWordCloud(WordCloudForm wordCloudForm, Integer id){
        if (!wordCloudForm.getWordclouddata().isEmpty()){
            WordCloud wc = createNewWordCloud(wordCloudForm, id);
            wordCloudRepo.save(wc);
        }
    }

    public List<WordCloud> findAllWordC(List<Activity> activity) {
        List<WordCloud> allClouds = new ArrayList<>();
        for(Activity a:activity) {
            WordCloud wcToAdd = wordCloudRepo.findWordCloudByActivityID(a);
            if (wcToAdd != null) {
                allClouds.add(wordCloudRepo.findWordCloudByActivityID(a));
            }
        }
        return allClouds;
    }
    public WordCloud viewCloud(int id){
        return wordCloudRepo.findWordCloudByWordCloudID(id);
    }
    public JSONArray viewFullCloud(int activID){
        WordCloud wordCloud = viewCloud(activID);
        JSONArray array = new JSONArray();

        if(wordCloud != null) {
            Map<String, Object> stuff = wordCloud.getWordCloudData();
            for(Map.Entry<String, Object> entry : stuff.entrySet()) {
                JSONObject record = new JSONObject();
                record.put("x", entry.getKey());
                record.put("value", entry.getValue());
                array.put(record);
            }

        } else {
            System.out.println("This is your problem");
        }

        return array;
    }

}
