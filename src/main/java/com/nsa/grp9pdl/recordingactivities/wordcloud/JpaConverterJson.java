package com.nsa.grp9pdl.recordingactivities.wordcloud;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.AttributeConverter;
import java.io.IOException;
import java.util.Map;


public class JpaConverterJson implements AttributeConverter<Map<String, Object>, String> {
    private final ObjectMapper objectMapper = new ObjectMapper();
    @Override
    public String convertToDatabaseColumn(Map<String, Object> wordCloudJson) {
        String wordCloudInfo = null;
        try {
            wordCloudInfo = objectMapper.writeValueAsString(wordCloudJson);
        } catch (final JsonProcessingException j) {
            System.out.println("JSON writing Error");
        }
        return wordCloudInfo;
    }

    @Override
    public Map<String, Object> convertToEntityAttribute(String wordCloudJsonDB) {
        Map<String, Object> wordCloud = null;
        try {
            wordCloud = objectMapper.readValue(wordCloudJsonDB, Map.class);
        } catch (final IOException i) {
            System.out.println("JSON reading error");
        }
        return wordCloud;
    }
}
