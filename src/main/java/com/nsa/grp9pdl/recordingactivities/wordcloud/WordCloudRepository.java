package com.nsa.grp9pdl.recordingactivities.wordcloud;

import com.nsa.grp9pdl.recordingactivities.activity.Activity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WordCloudRepository extends JpaRepository<WordCloud, Integer> {
    WordCloud findWordCloudByActivityID(Activity activityID);
    WordCloud findWordCloudByWordCloudID(int Id);
}
