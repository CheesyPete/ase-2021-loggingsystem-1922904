package com.nsa.grp9pdl.recordingactivities.restAPI;

import lombok.Data;

@Data
public class ActivityNumber {
    private Integer activityID;
}
