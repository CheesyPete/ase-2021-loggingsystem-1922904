package com.nsa.grp9pdl.recordingactivities.restAPI;

import java.util.ArrayList;
import java.util.List;

import com.nsa.grp9pdl.recordingactivities.grid.gridanswers.GridAnswersForm;
import com.nsa.grp9pdl.recordingactivities.grid.gridanswers.GridAnswersService;
import com.nsa.grp9pdl.recordingactivities.tagging.TaggingService;
import com.nsa.grp9pdl.recordingactivities.tagging.Tags;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ActivityRestController {
    @Autowired
    private TaggingService taggingService;

    @PostMapping(path = "/activity-tag-info", consumes = "application/json", produces = "application/json")
    public List<Tags> giveActivityTagInfo(@RequestBody ActivityNumber activity) {
        try {
            return taggingService.getTagsFromActivity(activity.getActivityID());
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }
}
