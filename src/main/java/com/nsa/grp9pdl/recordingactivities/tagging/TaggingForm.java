package com.nsa.grp9pdl.recordingactivities.tagging;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaggingForm {
    private boolean tag1;
    private boolean tag2;
    private boolean tag3;
    private boolean tag4;
    private boolean tag5;
    private boolean tag6;
    private boolean tag7;
    private boolean tag8;
    private boolean tag9;
    private boolean tag10;
    private boolean tag11;
    private boolean tag12;
    private boolean tag13;
    private boolean tag14;
    private boolean tag15;
    private boolean tag16;

    public boolean isTag1() {
        return tag1;
    }

    public void setTag1(boolean tag1) {
        this.tag1 = tag1;
    }

    public boolean isTag2() {
        return tag2;
    }

    public void setTag2(boolean tag2) {
        this.tag2 = tag2;
    }

    public boolean isTag3() {
        return tag3;
    }

    public void setTag3(boolean tag3) {
        this.tag3 = tag3;
    }

    public boolean isTag4() {
        return tag4;
    }

    public void setTag4(boolean tag4) {
        this.tag4 = tag4;
    }

    public boolean isTag5() {
        return tag5;
    }

    public void setTag5(boolean tag5) {
        this.tag5 = tag5;
    }

    public boolean isTag6() {
        return tag6;
    }

    public void setTag6(boolean tag6) {
        this.tag6 = tag6;
    }

    public boolean isTag7() {
        return tag7;
    }

    public void setTag7(boolean tag7) {
        this.tag7 = tag7;
    }

    public boolean isTag8() {
        return tag8;
    }

    public void setTag8(boolean tag8) {
        this.tag8 = tag8;
    }

    public boolean isTag9() {
        return tag9;
    }

    public void setTag9(boolean tag9) {
        this.tag9 = tag9;
    }

    public boolean isTag10() {
        return tag10;
    }

    public void setTag10(boolean tag10) {
        this.tag10 = tag10;
    }

    public boolean isTag11() {
        return tag11;
    }

    public void setTag11(boolean tag11) {
        this.tag11 = tag11;
    }

    public boolean isTag12() {
        return tag12;
    }

    public void setTag12(boolean tag12) {
        this.tag12 = tag12;
    }

    public boolean isTag13() {
        return tag13;
    }

    public void setTag13(boolean tag13) {
        this.tag13 = tag13;
    }

    public boolean isTag14() {
        return tag14;
    }

    public void setTag14(boolean tag14) {
        this.tag14 = tag14;
    }

    public boolean isTag15() {
        return tag15;
    }

    public void setTag15(boolean tag15) {
        this.tag15 = tag15;
    }

    public boolean isTag16() {
        return tag16;
    }

    public void setTag16(boolean tag16) {
        this.tag16 = tag16;
    }
}
