package com.nsa.grp9pdl.recordingactivities.tagging;

import com.nsa.grp9pdl.recordingactivities.activity.Activity;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "taglink")
public class TagLink {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TaglinkID")
    private Integer taglinkid;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="activityID", referencedColumnName = "activityID")
    private Activity activityID;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="tagID", referencedColumnName = "tagID")
    private Tags tagID;

}
