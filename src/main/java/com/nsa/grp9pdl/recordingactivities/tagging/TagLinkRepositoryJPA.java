package com.nsa.grp9pdl.recordingactivities.tagging;

import com.nsa.grp9pdl.recordingactivities.activity.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

import javax.transaction.Transactional;

@Repository
public interface TagLinkRepositoryJPA extends JpaRepository<TagLink, Integer>{
   List<TagLink> findAllByActivityID(Activity ID);
   Long countTagLinksByActivityID(Activity  ID);
   List<TagLink> findAllByTagID(Tags tagID);
   List<TagLink> findTagLinksByActivityID(Activity activity);
   @Transactional
   @Modifying
   @Query("DELETE FROM TagLink t WHERE t.activityID =:activity AND t.tagID=:tag")
   void deleteTagLinkByActivityIDAndTagID(@Param("activity") Activity activity, @Param("tag") Tags tag);
}
