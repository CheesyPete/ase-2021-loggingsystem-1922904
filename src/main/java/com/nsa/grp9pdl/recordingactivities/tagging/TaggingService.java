package com.nsa.grp9pdl.recordingactivities.tagging;

import com.nsa.grp9pdl.User.User;
import com.nsa.grp9pdl.recordingactivities.activity.Activity;
import com.nsa.grp9pdl.recordingactivities.activity.ActivityService;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

@Service
public class TaggingService {
    private final TaggingRepositoryJPA taggingRepository;
    private final ActivityService activityService;
    private final TagLinkRepositoryJPA tagLinkRepository;

    public TaggingService(TaggingRepositoryJPA taggingRepo, TagLinkRepositoryJPA tGR, ActivityService aS) {
        taggingRepository = taggingRepo;
        activityService = aS;
        tagLinkRepository = tGR;
    }

    public void saveNewTag(TaggingForm tagform, Integer id) {
        List<Boolean> tags = createNewTags(tagform);
        for (int i = 0; i < tags.size(); i++) {
            Boolean currentTag = tags.get(i);
            if (Boolean.TRUE.equals(currentTag)) {
                TagLink newTagLink = new TagLink();
                Activity actID = activityService.getActivity(id);
                Tags newTag = taggingRepository.findByTagID(i + 1);
                newTagLink.setActivityID(actID);
                newTagLink.setTagID(newTag);
                tagLinkRepository.save(newTagLink);
            }
        }
    }

    public List<TagDate> findTagsForUser(User UserID) {
        List<TagDate> tagDateList = new ArrayList<>();
        TagDate tagDate;

        List<Activity> activity = activityService.getActivitiesByUserID(UserID);
        for (Activity a : activity) {
            List<TagLink> linkedTags = tagLinkRepository.findAllByActivityID(a);

            for (TagLink t : linkedTags) {
                tagDate = new TagDate();
                tagDate.setTag(t.getTagID());
                tagDate.setDate(a.getDate());
                tagDateList.add(tagDate);
            }
        }

        return tagDateList;
    }

    public Map<Tags, Integer> findTotalTags(List<TagDate> tagDateList) {
        List<Tags> findall = taggingRepository.findAll();
        Map<Tags, Integer> tagMap = new HashMap<>();
        List<Tags> tagList = new ArrayList<>();

        for (Tags tag : findall) {
            tagMap.put(tag, 0);
        }

        tagDateList.stream().forEach(item -> tagList.add(item.getTag()));

        for (Tags tag : tagList) {
            Integer count = tagMap.get(tag);
            tagMap.put(tag, (count == null) ? 1 : count + 1);
        }

        return tagMap;
    }

    public List<Tags> getTagsFromActivity(Integer id) {
        Activity activity = activityService.getActivity(id);
        List<TagLink> linkedTags = tagLinkRepository.findAllByActivityID(activity);
        return getAllTagDetails(linkedTags);
    }

    public Map<Tags, List<Integer>> getTagIncreaseByDay(List<TagDate> userTags, int fromDaysAgo) {
        ArrayList<Date> dateList = findPastDates(fromDaysAgo);
        List<Tags> findall = taggingRepository.findAll();
        Map<Tags, List<Integer>> dailyTag = new HashMap<>();
        Map<Tags, Integer> tagTotals = findTotalTags(userTags);

        for (Tags tag : findall) {
            dailyTag.put(tag, new ArrayList<>());
        }

        for (Date date : dateList) {
            List<TagDate> tagsFromDate = getRecentTags(date, userTags);
            Map<Tags, Integer> recentTagTotals = findTotalTags(tagsFromDate);

            for (Map.Entry<Tags, Integer> dailyTagTotal : tagTotals.entrySet()) {
                Integer totalToDate = recentTagTotals.get(dailyTagTotal.getKey());
                if (totalToDate == null) {
                    totalToDate = 0;
                }

                Integer dailyTotal = dailyTagTotal.getValue() - totalToDate;
                List<Integer> currentList = dailyTag.get(dailyTagTotal.getKey());
                currentList.add(dailyTotal);
                dailyTag.put(dailyTagTotal.getKey(), currentList);
            }
        }

        return dailyTag;
    }

    private List<TagDate> getRecentTags(Date startDate, List<TagDate> userTags) {
        return userTags.stream().filter(tag -> tag.getDate().after(startDate)).collect(Collectors.toList());
    }

    public Date getLastDate(List<TagDate> userTags) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date temp = null;
        try {
            temp = sdf.parse("1970/01/01");
        } catch (ParseException ex) {
            System.err.println("eror parsing Date");
        }

        for (TagDate userTag: userTags) {
            if(temp.compareTo(userTag.getDate()) < 0) {
                temp = userTag.getDate();
            }
        }

        return temp;
    }

    public ArrayList<Date> findPastDates(int dateDifference) {
        LocalDateTime currentLocalDate = LocalDateTime.now();
        ArrayList<Date> dateList = new ArrayList<>();

        for (int i = 1; i <= dateDifference; i++) {
            LocalDateTime newLocalDate = currentLocalDate.minusDays(i);
            dateList.add(java.sql.Timestamp.valueOf(newLocalDate));
        }

        return dateList;
    }

    public int findTotalActivitiesForUser(User UserID) {
        List<Activity> activity = activityService.getActivitiesByUserID(UserID);
        return activity.size();
    }

    public int findTotalTagsForUser(List<TagDate> userTags) {
        return userTags.size();
    }

    private ArrayList<Boolean> createNewTags(TaggingForm taggingForm) {
        ArrayList<Boolean> listOfTags = new ArrayList<>();
        Boolean tag1 = taggingForm.isTag1();
        listOfTags.add(tag1);
        Boolean tag2 = taggingForm.isTag2();
        listOfTags.add(tag2);
        Boolean tag3 = taggingForm.isTag3();
        listOfTags.add(tag3);
        Boolean tag4 = taggingForm.isTag4();
        listOfTags.add(tag4);
        Boolean tag5 = taggingForm.isTag5();
        listOfTags.add(tag5);
        Boolean tag6 = taggingForm.isTag6();
        listOfTags.add(tag6);
        Boolean tag7 = taggingForm.isTag7();
        listOfTags.add(tag7);
        Boolean tag8 = taggingForm.isTag8();
        listOfTags.add(tag8);
        Boolean tag9 = taggingForm.isTag9();
        listOfTags.add(tag9);
        Boolean tag10 = taggingForm.isTag10();
        listOfTags.add(tag10);
        Boolean tag11 = taggingForm.isTag11();
        listOfTags.add(tag11);
        Boolean tag12 = taggingForm.isTag12();
        listOfTags.add(tag12);
        Boolean tag13 = taggingForm.isTag13();
        listOfTags.add(tag13);
        Boolean tag14 = taggingForm.isTag14();
        listOfTags.add(tag14);
        Boolean tag15 = taggingForm.isTag15();
        listOfTags.add(tag15);
        Boolean tag16 = taggingForm.isTag16();
        listOfTags.add(tag16);

        return listOfTags;

    }

    public List<TagLink> getAllTags(int id) {
        Activity activity = activityService.getActivity(id);
        return tagLinkRepository.findTagLinksByActivityID(activity);
    }

    public List<Tags> getAllTagDetails(List<TagLink> tags) {
        List<Tags> newTags = new ArrayList<>(tags.size());
        for (TagLink t : tags) {
            Tags nT = t.getTagID();
            newTags.add(nT);
        }
        return newTags;
    }

    public int findTotalTagsForUser(User UserID) {
        List<Tags> findall = taggingRepository.findAll();
        Map<Tags, Integer> TagHash = new HashMap<>();

        for (Tags tag : findall) {
            tagLinkRepository.findAllByTagID(tag);
            TagHash.put(tag, 0);
        }

        List<Activity> activity = activityService.getActivitiesByUserID(UserID);
        int Total = 0;
        for (Activity a : activity) {
            List<TagLink> linkedTags = tagLinkRepository.findAllByActivityID(a);

            for (TagLink t : linkedTags) {
                if (TagHash.containsKey(t.getTagID())) {
                    Total++;
                }
            }
        }

        return Total;
    }

    public TaggingForm getFormBasedOnTagging(Integer id) {
        Activity activity = activityService.getActivity(id);
        List<TagLink> tagLinks = tagLinkRepository.findAllByActivityID(activity);
        TaggingForm tForm = new TaggingForm();
        if (tagLinks != null || !tagLinks.isEmpty()) {
            for (TagLink t : tagLinks) {
                Tags tag = t.getTagID();
                Integer tid = tag.getTagID();
                setCheckBox(tid, tForm);
            }
        }
        return tForm;
    }

    public void setCheckBox(Integer id, TaggingForm tf) {
        switch (id) {
            case 1:
                tf.setTag1(true);
                break;
            case 2:
                tf.setTag2(true);
                break;
            case 3:
                tf.setTag3(true);
                break;
            case 4:
                tf.setTag4(true);
                break;
            case 5:
                tf.setTag5(true);
                break;
            case 6:
                tf.setTag6(true);
                break;
            case 7:
                tf.setTag7(true);
                break;
            case 8:
                tf.setTag8(true);
                break;
            case 9:
                tf.setTag9(true);
                break;
            case 10:
                tf.setTag10(true);
                break;
            case 11:
                tf.setTag11(true);
                break;
            case 12:
                tf.setTag12(true);
                break;
            case 13:
                tf.setTag13(true);
                break;
            case 14:
                tf.setTag14(true);
                break;
            case 15:
                tf.setTag15(true);
                break;
            case 16:
                tf.setTag16(true);
                break;
        }
    }

    @Transactional
    public void updateTags(TaggingForm tags, Integer id) {
        TaggingForm tf = getFormBasedOnTagging(id);
        ArrayList<Boolean> oldTagList = createNewTags(tf);
        ArrayList<Boolean> newTagList = createNewTags(tags);
        Activity activity = activityService.getActivity(id);
        for (int i = 0; i < oldTagList.size(); i++) {
            if (oldTagList.get(i) != newTagList.get(i)) {
                Integer tid = i + 1;
                Tags updateTag = taggingRepository.findByTagID(tid);
                if (oldTagList.get(i) && !newTagList.get(i)) {
                    tagLinkRepository.deleteTagLinkByActivityIDAndTagID(activity, updateTag);
                } else {
                    TagLink newTagLink = new TagLink();
                    newTagLink.setActivityID(activity);
                    newTagLink.setTagID(updateTag);
                    tagLinkRepository.save(newTagLink);
                }
            } else {
                System.out.println("Match!");
            }
        }

    }
}
