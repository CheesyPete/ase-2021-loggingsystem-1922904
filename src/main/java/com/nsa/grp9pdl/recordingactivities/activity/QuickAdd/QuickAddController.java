package com.nsa.grp9pdl.recordingactivities.activity.QuickAdd;

import com.nsa.grp9pdl.recordingactivities.activity.Activity;
import com.nsa.grp9pdl.recordingactivities.activity.ActivityService;
import com.nsa.grp9pdl.recordingactivities.keydetails.KeyDetailsForm;
import com.nsa.grp9pdl.security.CustomAuthenticationSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class QuickAddController {
    @Autowired
    private ActivityService activityService;

    @GetMapping("/new-activity/quick-add")
    public String showForm(Model model) {
        QuickAddForm quickAddForm = new QuickAddForm();
        model.addAttribute("qaForm", quickAddForm);
        return "quick-add";
    }

    @PostMapping("new-activity/quick-add/dashboard")
    public String getForm(QuickAddForm quickAddForm, BindingResult bindings) {

        activityService.createNewActivity(quickAddForm.getDate(), quickAddForm.getTitle(), quickAddForm.getLocation());

        return "redirect:/dashboard";
    }

    @PostMapping("new-activity/quick-add/wordcloud")
    public String getForm2(QuickAddForm quickAddForm, BindingResult bindings) {

        int id = activityService.createNewActivity(quickAddForm.getDate(), quickAddForm.getTitle(),
                quickAddForm.getLocation());

        return "redirect:/wordcloud/" + id;
    }

    @PostMapping("new-activity/quick-add/grid-reflection")
    public String getForm3(QuickAddForm quickAddForm, BindingResult bindings) {
        int id = activityService.createNewActivity(quickAddForm.getDate(), quickAddForm.getTitle(),
                quickAddForm.getLocation());

        return "redirect:/grid-reflection/" + id;
    }
    @PostMapping("new-activity/dimensions")
    public String getForm4(QuickAddForm quickAddForm, BindingResult bindings) {
        int id = activityService.createNewActivity(quickAddForm.getDate(), quickAddForm.getTitle(),
                quickAddForm.getLocation());

        return "redirect:dimensions/"+ id;
    }

}
