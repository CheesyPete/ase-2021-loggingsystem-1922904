package com.nsa.grp9pdl.recordingactivities.activity;

import com.nsa.grp9pdl.recordingactivities.tagging.TagLink;
import com.nsa.grp9pdl.recordingactivities.tagging.TagLinkRepositoryJPA;
import com.nsa.grp9pdl.User.User;
import com.nsa.grp9pdl.User.UserRepositoryJPA;
import com.nsa.grp9pdl.recordingactivities.keydetails.KeyDetails;
import com.nsa.grp9pdl.recordingactivities.keydetails.KeyDetailsRepo;
import com.nsa.grp9pdl.recordingactivities.reflections.Reflections;
import com.nsa.grp9pdl.recordingactivities.reflections.ReflectionsRepo;
import com.nsa.grp9pdl.security.CustomAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Stream;

@Component
public class ActivityService implements ActivityAuditor {
    private final ActivityRepositoryJPA activityRepo;
    private final UserRepositoryJPA userRepo;
    private final TagLinkRepositoryJPA taggingRepo;
    private final ReflectionsRepo reflectionsRepo;
    private final KeyDetailsRepo keydetailsRepo;

    public ActivityService(ActivityRepositoryJPA aR, UserRepositoryJPA uR, TagLinkRepositoryJPA tR, ReflectionsRepo rR, KeyDetailsRepo kd) {
        activityRepo = aR;
        userRepo = uR;
        taggingRepo = tR;
        reflectionsRepo = rR;
        keydetailsRepo = kd;
    }

    @Override
    public Integer createNewActivity(Date date, String title, String location) {

        Activity newActivity = new Activity();
        Date passedDate = date;
        String passedTitle = title;
        String passedLocation = location;


        User newUser = userRepo.findUserById(CustomAuthenticationSuccessHandler.currentUserDetails().getUserID());
        newActivity.setDate(passedDate);
        newActivity.setUserID(newUser);
        newActivity.setTitle(passedTitle);
        newActivity.setLocation(passedLocation);
        activityRepo.save(newActivity);

        return newActivity.getActivityID();
    }

    public Activity getActivity(Integer id) {
        Activity currentAct = activityRepo.getActivityByActivityID(id);
        return currentAct;
    }
    public List<Activity> getAllActivities() {
        User newUser = userRepo.findUserById(CustomAuthenticationSuccessHandler.currentUserDetails().getUserID());
        return activityRepo.getActivitiesByUserID(newUser);
    }
    public List<Integer> getActivityPercentage(List<Activity> activities){
        List<Integer> percentages = new ArrayList<>(activities.size());
        for(int i =0; i < activities.size(); i++) {
            Activity activity = activityRepo.getActivityByActivityID(i + 1);
            Reflections testR = reflectionsRepo.findByActivityID(activity);
            KeyDetails testK = keydetailsRepo.findByActivityID(activity);
            List<TagLink> testT = taggingRepo.findTagLinksByActivityID(activity);
            if (areAllNull(testK, testR) && testT.isEmpty()) {
                percentages.add(0);
            } else if (areAllNotNull(testK, testR) && !testT.isEmpty()) {
                percentages.add(100);
            } else if ((areAllNotNull(testK) && areAllNull(testR) && testT.isEmpty()) || (areAllNotNull(testR) && areAllNull(testK) && testT.isEmpty()) || (!testT.isEmpty() && areAllNull(testK, testR))) {
                percentages.add(33);
            } else if ((areAllNotNull(testK, testR) && testT.isEmpty()) || (areAllNotNull(testK) && areAllNull(testR) && !testT.isEmpty()) || (areAllNotNull(testR) && areAllNull(testK) && !testT.isEmpty())) {
                percentages.add(66);
            }
        }
        return percentages;
    }
    private boolean areAllNull(Object... objects){
        return Stream.of(objects).allMatch(Objects::isNull);
    }
    private boolean areAllNotNull(Object... objects){
        return Stream.of(objects).allMatch(Objects::nonNull);
    }

    public List<Activity>  getActivitiesByUserID(User UserID){
        return activityRepo.findActivityByUserID(UserID);
    }

}
