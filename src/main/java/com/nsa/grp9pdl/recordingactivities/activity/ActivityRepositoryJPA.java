package com.nsa.grp9pdl.recordingactivities.activity;

import com.nsa.grp9pdl.User.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ActivityRepositoryJPA extends JpaRepository<Activity, Integer> {
    List<Activity> findActivityByUserID (User userID);
    List<Activity> getActivitiesByUserID(User userID);


    Activity getActivityByActivityID(Integer id);

    List<Activity> findAllByUserID(User userID);
}
