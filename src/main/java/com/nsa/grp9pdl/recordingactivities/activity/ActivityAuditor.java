package com.nsa.grp9pdl.recordingactivities.activity;

import java.util.Date;

public interface ActivityAuditor {
    Integer createNewActivity(Date date, String title, String location);
}
