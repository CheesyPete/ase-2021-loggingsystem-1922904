package com.nsa.grp9pdl.recordingactivities.activity.QuickAdd;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuickAddForm {

    private String title;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;
    private String location;

}

