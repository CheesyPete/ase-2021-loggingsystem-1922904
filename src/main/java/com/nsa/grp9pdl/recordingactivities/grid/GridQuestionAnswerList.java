package com.nsa.grp9pdl.recordingactivities.grid;

import java.util.ArrayList;
import java.util.List;

import com.nsa.grp9pdl.recordingactivities.grid.gridanswers.GridAnswers;
import com.nsa.grp9pdl.recordingactivities.grid.gridquestions.GridQuestions;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class GridQuestionAnswerList {
    private List<GridQuestionAnswerPair> gridQuestionAnswerPairList = new ArrayList<>();
    @Setter private List<GridQuestions> questionList;
    @Setter private List<GridAnswers> answerList;

    public void build() {
        for (int i = 0; i < answerList.size(); i++) {
            GridQuestionAnswerPair gridQuestionAnswerPair = new GridQuestionAnswerPair();
            GridAnswers answer = answerList.get(i);
            GridQuestions question = questionList.get(answer.getGridquestionID().getGridquestionID()-1);

            gridQuestionAnswerPair.setQuestion(question.getQuestion());
            gridQuestionAnswerPair.setAnswer(answer.getAnswer());
            gridQuestionAnswerPairList.add(gridQuestionAnswerPair);
        }
    }
}
