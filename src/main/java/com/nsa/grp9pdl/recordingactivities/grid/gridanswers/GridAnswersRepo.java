package com.nsa.grp9pdl.recordingactivities.grid.gridanswers;

import java.util.List;

import com.nsa.grp9pdl.recordingactivities.activity.Activity;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GridAnswersRepo extends JpaRepository<GridAnswers, Integer> {
    List<GridAnswers> findAllByActivityID(Activity activity);
    
}
