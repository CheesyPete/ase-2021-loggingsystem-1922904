package com.nsa.grp9pdl.recordingactivities.grid;

import lombok.Data;
import lombok.Setter;

@Data
public class GridQuestionAnswerPair {
    @Setter String question;
    @Setter String answer;
}
