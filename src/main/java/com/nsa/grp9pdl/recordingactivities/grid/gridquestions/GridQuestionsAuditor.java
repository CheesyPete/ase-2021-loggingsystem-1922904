package com.nsa.grp9pdl.recordingactivities.grid.gridquestions;

import java.util.List;

public interface GridQuestionsAuditor {
    GridQuestions getGridQuestions(Integer id);
    List<GridQuestions> findAll();
}
