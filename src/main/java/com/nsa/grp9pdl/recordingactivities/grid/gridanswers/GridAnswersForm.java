package com.nsa.grp9pdl.recordingactivities.grid.gridanswers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GridAnswersForm {

    @NotEmpty
    @NotNull
    @Size(max = 255)
    private String answer;

}
