package com.nsa.grp9pdl.recordingactivities.grid.gridquestions;

import org.springframework.data.jpa.repository.JpaRepository;


public interface GridQuestionsRepo extends JpaRepository<GridQuestions, Integer> {
    GridQuestions getGridQuestionsByGridquestionID(Integer id);
}
