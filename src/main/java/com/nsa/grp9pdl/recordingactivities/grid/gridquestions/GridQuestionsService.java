package com.nsa.grp9pdl.recordingactivities.grid.gridquestions;

import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

@Service
@Configuration
public class GridQuestionsService implements GridQuestionsAuditor {
    private final GridQuestionsRepo gridQuestionsRepo;

    public GridQuestionsService( GridQuestionsRepo gqR) {
        gridQuestionsRepo = gqR;
    }

    @Override
    public GridQuestions getGridQuestions(Integer id) {
        return gridQuestionsRepo.getGridQuestionsByGridquestionID(id);
    }

    @Override
    public List<GridQuestions> findAll() {
        return gridQuestionsRepo.findAll();
    }
}
