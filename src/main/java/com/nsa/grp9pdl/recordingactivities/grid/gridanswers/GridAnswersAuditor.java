package com.nsa.grp9pdl.recordingactivities.grid.gridanswers;

import java.util.List;

public interface GridAnswersAuditor {
    void saveGridAnswers(GridAnswersForm newReflection, Integer aID, Integer gID);
    List<GridAnswers> findByActivityID(Integer id);
}
