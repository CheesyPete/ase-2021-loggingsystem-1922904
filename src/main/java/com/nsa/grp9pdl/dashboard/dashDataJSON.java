package com.nsa.grp9pdl.dashboard;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.nsa.grp9pdl.recordingactivities.tagging.Tags;

import org.json.JSONObject;

public class dashDataJSON {

    public List<Object> lineGraphJSON(Map<Tags, List<Integer>> tagFrequency, ArrayList<Date> dateList) {
        List<Object> lineListJSON = new ArrayList<>();
        JSONObject lineItem;

        for (int i = 0; i < dateList.size(); i++) {
            lineItem = new JSONObject();

            lineItem.put("y", dateList.get(i).toString());
            
            for (Map.Entry<Tags, List<Integer>> tagPair : tagFrequency.entrySet()) {
                lineItem.put(tagPair.getKey().getName(), tagPair.getValue().get(i));
            }
            lineListJSON.add(lineItem);
        }

        return lineListJSON;
    }

    public List<String> getLineTagNames(Map<Tags, List<Integer>> tagFrequency) {
        List<String> tagNameList = new ArrayList<>();

        tagFrequency.forEach((tag, freq) -> tagNameList.add('"' + tag.getName() + '"'));

        return tagNameList;
    }

    private List<Object> donutChartJSON(Map<String, Integer> tagList) {
        List<Object> donutChartJSON = new ArrayList<>();
        JSONObject sectionItem;

        for (String i : tagList.keySet()) {
            sectionItem = new JSONObject();

            sectionItem.put("label", i);
            sectionItem.put("value", tagList.get(i));
            donutChartJSON.add(sectionItem);
        }

        return donutChartJSON;
    }

    public List<Object> donutChart1JSON(Map<Tags, Integer> tagList) {
        Map<String, Integer> selectSet = tagList.entrySet().stream()
                .filter(tag -> tag.getKey().getName().startsWith("A") || tag.getKey().getName().startsWith("D"))
                .collect(Collectors.toMap(tag -> tag.getKey().getName(), tag -> tag.getValue()));

        return donutChartJSON(selectSet);
    }

    public List<Object> donutChart2JSON(Map<Tags, Integer> tagList) {
        Map<String, Integer> selectSet = tagList.entrySet().stream()
                .filter(tag -> tag.getKey().getName().startsWith("K"))
                .collect(Collectors.toMap(tag -> tag.getKey().getName(), tag -> tag.getValue()));

        return donutChartJSON(selectSet);
    }

    public List<Object> donutChart3JSON(Map<Tags, Integer> tagList) {
        Map<String, Integer> selectSet = tagList.entrySet().stream()
                .filter(tag -> tag.getKey().getName().startsWith("V"))
                .collect(Collectors.toMap(tag -> tag.getKey().getName(), tag -> tag.getValue()));

        return donutChartJSON(selectSet);
    }
}
