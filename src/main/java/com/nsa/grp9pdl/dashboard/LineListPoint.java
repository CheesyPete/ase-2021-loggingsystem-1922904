package com.nsa.grp9pdl.dashboard;

import java.util.Random;

import lombok.Data;

@Data
public class LineListPoint {
    private String date;
    private Integer a1Total;
    private Integer a2Total;

    public LineListPoint createData(String inDate) {
        Random random = new Random();

        date = inDate;
        a1Total = random.nextInt(11);
        a2Total = random.nextInt(11);

        return this;
    }
}
