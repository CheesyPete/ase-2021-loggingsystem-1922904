package com.nsa.grp9pdl.exporting;

import com.lowagie.text.Document;
import com.lowagie.text.Table;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Cell;
import com.lowagie.text.pdf.PdfWriter;
import com.nsa.grp9pdl.User.User;
import com.nsa.grp9pdl.User.UserRepositoryJPA;
import com.nsa.grp9pdl.recordingactivities.activity.Activity;
import com.nsa.grp9pdl.recordingactivities.keydetails.KeyDetails;
import com.nsa.grp9pdl.recordingactivities.keydetails.KeyDetailsService;
import com.nsa.grp9pdl.recordingactivities.reflections.Reflections;
import com.nsa.grp9pdl.recordingactivities.reflections.ReflectionsService;
import com.nsa.grp9pdl.recordingactivities.tagging.TagDate;
import com.nsa.grp9pdl.recordingactivities.tagging.TaggingService;
import com.nsa.grp9pdl.recordingactivities.tagging.Tags;
import com.nsa.grp9pdl.security.CustomAuthenticationSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class ExportToPdf {
    @Autowired
    private final KeyDetailsService keyDetails;
    @Autowired
    private final ReflectionsService reflections;
    @Autowired
    private final TaggingService tagsS;
    @Autowired
    private final UserRepositoryJPA userRepositoryJPA;
    @Value("${encryption.key}")
    private String secretKey;

    public ExportToPdf(KeyDetailsService kd, ReflectionsService rf, TaggingService tgs, UserRepositoryJPA urjpa) {
        this.keyDetails = kd;
        reflections = rf;
        tagsS = tgs;
        userRepositoryJPA = urjpa;
    }

    public void export(HttpServletResponse response) throws DocumentException, IOException {
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, response.getOutputStream());
        document.open();
        Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        font.setSize(22);
        String username = CustomAuthenticationSuccessHandler.currentUserDetails().getUsername();
        String title = "Professional Development System - " + username + "'s Data";
        Paragraph p = new Paragraph(title, font);
        p.setAlignment(Paragraph.ALIGN_CENTER);
        document.add(p);
        Integer userID = CustomAuthenticationSuccessHandler.currentUserDetails().getUserID();
        User aUsers = userRepositoryJPA.findUserById(userID);
        List<TagDate> userTags = tagsS.findTagsForUser(aUsers);
        int activityTotal = tagsS.findTotalActivitiesForUser(aUsers);
        int tagTotal = tagsS.findTotalTagsForUser(userTags);
        String acSubmitted = "Activities Submitted: " + activityTotal;
        String aTags = "Achieved Tags: " + tagTotal;
        p = new Paragraph(acSubmitted);
        document.add(p);
        p = new Paragraph(aTags);
        document.add(p);
        Table atable = getAreasOfActivityTags(userTags);
        Table table2 = getCoreKnowledgeTags(userTags);
        Table table3 = getProfessionalValuesTags(userTags);
        document.add(atable);
        document.add(table2);
        document.add(table3);
        document.close();
    }

    public void export(HttpServletResponse response, Activity activity, String d, String oi) throws DocumentException, IOException {
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, response.getOutputStream());
        document.open();
        String title = getTitle(activity);
        String author = getAuthor(activity);
        Paragraph p = new Paragraph(title);
        Paragraph p2 = new Paragraph(author);
        Table kdtable = getKeyDetails(activity, d, oi);
        Table rtable = getReflections(activity);
        Table tTable = getTags(activity);
        document.add(p);
        document.add(p2);
        document.add(kdtable);
        document.add(rtable);
        document.add(tTable);
        document.close();

    }
    private String getTitle(Activity activity) {
        return activity.getTitle() + " - " + activity.getDate();
    }
    private String getAuthor(Activity activity) {
        User user = activity.getUserID();
        return user.getUsername();
    }
    private Table getKeyDetails(Activity activity, String d, String oi) {
        KeyDetails kd = keyDetails.getKeyDetailsForActivity(activity.getActivityID());
        Table table = new Table(2);
        table.setBorderWidth(1);
        table.setPadding(5);
        table.setSpacing(5);
        Cell cell = new Cell("Key Details");
        cell.setHeader(true);
        cell.setColspan(2);
        table.addCell(cell);
        table.endHeaders();
        cell = new Cell("Description");
        table.addCell(cell);
        cell = new Cell(d);
        table.addCell(cell);
        cell = new Cell("Role: ");
        table.addCell(cell);
        cell = new Cell(kd.getRole());
        table.addCell(cell);
        cell = new Cell("Others Involved: ");
        table.addCell(cell);
        cell = new Cell(oi);
        table.addCell(cell);
        cell = new Cell("The impact it had:");
        table.addCell(cell);
        cell = new Cell(kd.getImpact());
        table.addCell(cell);
        cell = new Cell("Learning Technology Used:");
        table.addCell(cell);
        cell = new Cell(kd.getLearningTech());
        table.addCell(cell);
        return table;
    }
    private Table getReflections(Activity activity) {
        int id = activity.getActivityID();
        Reflections r = reflections.findReflections(id);
        Table table = new Table(2);
        table.setBorderWidth(1);
        table.setPadding(5);
        table.setSpacing(5);
        Cell cell = new Cell("Reflection");
        cell.setHeader(true);
        cell.setColspan(2);
        table.addCell(cell);
        table.endHeaders();
        cell = new Cell("Approach: ");
        table.addCell(cell);
        cell = new Cell(r.getApproach());
        table.addCell(cell);
        cell = new Cell("Happenings: ");
        table.addCell(cell);
        cell = new Cell(r.getHappenings());
        table.addCell(cell);
        cell = new Cell("Evaluation: ");
        table.addCell(cell);
        cell = new Cell(r.getEvaluation());
        table.addCell(cell);
        if(r.isNeedEvidence()) {
            cell = new Cell("Evidence :");
            table.addCell(cell);
            cell = new Cell(r.getEvidence());
            table.addCell(cell);
        }
        cell = new Cell("Even better if: ");
        table.addCell(cell);
        cell = new Cell(r.getEbi());
        table.addCell(cell);
        return table;
    }
    private Table getTags(Activity activity) {
        Integer id = activity.getActivityID();
        List<Tags> tags = tagsS.getTagsFromActivity(id);
        Table table = new Table(1);
        table.setBorderWidth(1);
        table.setPadding(5);
        table.setSpacing(5);
        Cell cell = new Cell("Tags");
        cell.setHeader(true);
        table.addCell(cell);
        table.endHeaders();
        for(Tags t: tags) {
            String tagToAdd = t.getName() + ": "+t.getDescription();
            cell = new Cell(tagToAdd);
            table.addCell(cell);
        }
        return table;
    }
    private Table getAreasOfActivityTags(List<TagDate> userTags){
        Table table = new Table(2);
        table.setBorderWidth(1);
        table.setPadding(5);
        table.setSpacing(5);
        Cell cell = new Cell("Area of Activity Tags");
        cell.setHeader(true);
        cell.setColspan(2);
        table.addCell(cell);
        table.endHeaders();
        Map<Tags, Integer> userTagTotals = tagsS.findTotalTags(userTags);
        //Taken from Adam's work in dashDataJSON.java - donutChart1JSON().
        Map<String, Integer> selectSet = userTagTotals.entrySet().stream()
                .filter(tag -> tag.getKey().getName().startsWith("A") || tag.getKey().getName().startsWith("D"))
                .collect(Collectors.toMap(tag -> tag.getKey().getName(), tag -> tag.getValue()));
        for(Map.Entry<String, Integer> entry : selectSet.entrySet()) {
            cell = new Cell(entry.getKey());
            table.addCell(cell);
            cell = new Cell(String.valueOf(entry.getValue()));
            table.addCell(cell);
        }
        return table;
    }
    private Table getCoreKnowledgeTags(List<TagDate> userTags){
        Table table = new Table(2);
        table.setBorderWidth(1);
        table.setPadding(5);
        table.setSpacing(5);
        Cell cell = new Cell("Core Knowledge Tags");
        cell.setHeader(true);
        cell.setColspan(2);
        table.addCell(cell);
        table.endHeaders();
        Map<Tags, Integer> userTagTotals = tagsS.findTotalTags(userTags);
        //Taken from Adam's work in dashDataJSON.java - donutChart1JSON().
        Map<String, Integer> selectSet = userTagTotals.entrySet().stream()
                .filter(tag -> tag.getKey().getName().startsWith("K") || tag.getKey().getName().startsWith("D"))
                .collect(Collectors.toMap(tag -> tag.getKey().getName(), tag -> tag.getValue()));
        for(Map.Entry<String, Integer> entry : selectSet.entrySet()) {
            cell = new Cell(entry.getKey());
            table.addCell(cell);
            cell = new Cell(String.valueOf(entry.getValue()));
            table.addCell(cell);
        }
        return table;
    }
    private Table getProfessionalValuesTags(List<TagDate> userTags){
        Table table = new Table(2);
        table.setBorderWidth(1);
        table.setPadding(5);
        table.setSpacing(5);
        Cell cell = new Cell("Professional Value Tags");
        cell.setHeader(true);
        cell.setColspan(2);
        table.addCell(cell);
        table.endHeaders();
        Map<Tags, Integer> userTagTotals = tagsS.findTotalTags(userTags);
        //Taken from Adam's work in dashDataJSON.java - donutChart1JSON().
        Map<String, Integer> selectSet = userTagTotals.entrySet().stream()
                .filter(tag -> tag.getKey().getName().startsWith("V") || tag.getKey().getName().startsWith("D"))
                .collect(Collectors.toMap(tag -> tag.getKey().getName(), tag -> tag.getValue()));
        for(Map.Entry<String, Integer> entry : selectSet.entrySet()) {
            cell = new Cell(entry.getKey());
            table.addCell(cell);
            cell = new Cell(String.valueOf(entry.getValue()));
            table.addCell(cell);
        }
        return table;
    }
}
