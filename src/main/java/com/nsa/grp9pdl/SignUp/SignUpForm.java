package com.nsa.grp9pdl.SignUp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignUpForm {
    @NotEmpty
    @Size(max = 50)
    private String email;

    @NotEmpty
    private String username;

    @NotEmpty
    @Size(min = 8, max = 50)
    private String password;

    private String Role = "user";
}
