package com.nsa.grp9pdl.ToDo;

import com.nsa.grp9pdl.User.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ToDoRepositoryJPA extends JpaRepository<ToDo, Integer> {
    List<ToDo> findAllByUserIDAndCompleted(User userID,boolean completed);
    ToDo findByTodoID(int id);
}


