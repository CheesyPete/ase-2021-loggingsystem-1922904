package com.nsa.grp9pdl.ToDo;

import lombok.Data;

import java.util.List;

@Data
public class SubmittedCheckboxes {
    private int id;
}
