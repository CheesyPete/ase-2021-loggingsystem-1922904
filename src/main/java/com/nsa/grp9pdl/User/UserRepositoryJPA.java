package com.nsa.grp9pdl.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepositoryJPA extends JpaRepository<User, Integer> {
    User findByUsername(String aUsername);
    User findUserById(Integer id);

    List<User> findById(String aUsername);

    List<User> findByUsernameOrderById(String aUsername);
}
