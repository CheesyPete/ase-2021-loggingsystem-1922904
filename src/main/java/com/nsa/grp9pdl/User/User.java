package com.nsa.grp9pdl.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "User")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "userid")
    private Integer id;
    // Stringchosen for email as its hard to represent the variety of characters needed in any other data type
    // @NotEmpty
    // @NotNull
    // @Max(50)
    private String email;
    // @NotEmpty
    // @NotNull
    // @Max(32)
    // @Min(8)
    //    String chosen for username as its hard to represent the variety of characters needed in any other data type
    private String username;
    // @NotEmpty
    // @NotNull
    // String chosen for Password as its hard to represent the variety of characters needed in any other data type
    private String password;

    private String role = "user";


}
