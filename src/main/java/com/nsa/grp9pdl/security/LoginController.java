package com.nsa.grp9pdl.security;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {
    @GetMapping("/login")
    public String goToLogin() {
        return "login";
    }

    @GetMapping("/")
    public String goToHome() {
        return "redirect:/dashboard";
    }
}
