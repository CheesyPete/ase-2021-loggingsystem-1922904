-- Initial SQL script for client project

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema clientproject
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `clientproject`;
USE `clientproject` ;

-- -----------------------------------------------------
-- Table `clientproject`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clientproject`.`user` (
  `userID` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(255) NOT NULL,
  `username` VARCHAR(16) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `role` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`userID`));

-- -----------------------------------------------------
-- Table `clientproject`.`activities`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clientproject`.`activities` (
  `activityID` INT NOT NULL AUTO_INCREMENT,
  `userID` INT NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `location` VARCHAR(50) NOT NULL,
  `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`activityID`),
  FOREIGN KEY (`userID`)
    REFERENCES `clientproject`.`user` (`userID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
  );

-- -----------------------------------------------------
-- Table `clientproject.keyDetails
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `keydetails` (
    `activityID` INT NOT NULL,
    `keyDetailsID` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `description` VARCHAR(255),
    `role` VARCHAR(255),
    `othersInvolved` VARCHAR(255),
    `impact` VARCHAR(255),
    `learningTech` VARCHAR(255) ,
    FOREIGN KEY(`activityID`) REFERENCES activities(activityID)
);

-- -----------------------------------------------------
-- Table `clientproject`.`reflections`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `reflections`(
    `activityID` INT NOT NULL,
    `reflectionsID` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `title` VARCHAR(255),
    `approach` VARCHAR(255),
    `happenings` VARCHAR(255),
    `evaluation` VARCHAR(255),
    `needEvidence` BOOLEAN,
    `evidence` VARCHAR(255) DEFAULT 'N/A',
    `ebi` VARCHAR(255),
    `learningPoints` VARCHAR(255)
);

-- -----------------------------------------------------
-- Table `clientproject`.`tags`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clientproject`.`tags` (
  `tagID` INT UNIQUE NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(225) NOT NULL,
  `description` VARCHAR(225) NOT NULL,
  PRIMARY KEY (`tagID`));


-- -----------------------------------------------------
-- Table `clientproject`.`taglink`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clientproject`.`taglink` (
  `tagLinkID` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `activityID` INT NOT NULL,
  `tagID` INT NOT NULL,

  FOREIGN KEY (`activityID`)
    REFERENCES `clientproject`.`activities` (`activityID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    FOREIGN KEY (`tagID`)
    REFERENCES `clientproject`.`tags` (`tagID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

-- -----------------------------------------------------
-- Table `clientproject`.`wordcloud`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wordcloud` (
`wordCloudID` INT NOT NULL AUTO_INCREMENT,
  `activityID` INT NOT NULL,
  `wordCloudData` JSON NOT NULL,
  PRIMARY KEY(`wordCloudID`),
  FOREIGN KEY(`activityID`) REFERENCES activities(`activityID`)
);

-- -----------------------------------------------------
-- Table `clientproject`.`gridquestions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gridquestions` (
`gridquestionID` INT NOT NULL AUTO_INCREMENT,
  `question` VARCHAR(225),
  PRIMARY KEY(`gridquestionID`)
);

-- -----------------------------------------------------
-- Table `clientproject`.`gridanswers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gridanswers` (
`gridanswerID` INT NOT NULL AUTO_INCREMENT,
  `gridquestionID` INT NOT NULL,
  `activityID` INT NOT NULL,
  `answer` VARCHAR(225) NOT NULL,
  PRIMARY KEY(`gridanswerID`),
  FOREIGN KEY(`gridquestionID`) REFERENCES gridquestions(`gridquestionID`),
  FOREIGN KEY(`activityID`) REFERENCES activities(`activityID`)
);

INSERT INTO `gridquestions`(question) VALUES("How do you feel");
INSERT INTO `gridquestions`(question) VALUES("Were you comfortable");
INSERT INTO `gridquestions`(question) VALUES("What do you think of the time");
INSERT INTO `gridquestions`(question) VALUES("What did you eat");
INSERT INTO `gridquestions`(question) VALUES("How long did it take");
INSERT INTO `gridquestions`(question) VALUES("Were you interested");
INSERT INTO `gridquestions`(question) VALUES("Did you learn something");
INSERT INTO `gridquestions`(question) VALUES("How will you improve");
INSERT INTO `gridquestions`(question) VALUES("What do you enjoy about the event");
INSERT INTO `gridquestions`(question) VALUES("Would you do it again");
INSERT INTO `gridquestions`(question) VALUES("Did you understand what was said");
INSERT INTO `gridquestions`(question) VALUES("Was it worth it");

-- -----------------------------------------------------
-- Table `clientproject`.`todo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `clientproject`.`todo` (
  `todoid` INT UNIQUE NOT NULL AUTO_INCREMENT,
  `Name` VARCHAR(225) NOT NULL,
  `completed` BOOLEAN,
  `UserID` INT NOT NULL,
   PRIMARY KEY (`todoid`),
	FOREIGN KEY (`UserID`)
    REFERENCES `clientproject`.`user` (`UserID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
  );

INSERT INTO `clientproject`.`todo`(Name, Completed ,UserID) VALUES ("Write Reflective Essay",false, 1);
INSERT INTO `clientproject`.`todo`(Name, Completed ,UserID) VALUES ("Attend Teaching Conference",false, 1);
INSERT INTO `clientproject`.`todo`(Name, Completed ,UserID) VALUES ("Read Diversity Pamphlet",false, 1);

ALTER TABLE `tags` ADD COLUMN `Description` VARCHAR(255) AFTER `Name`;
INSERT INTO `tags`(Name, Description) VALUES("A1", "Design and plan learning activities and/or programmes of study");
INSERT INTO `tags`(Name, Description) VALUES("A2", "Teach and/or support learning.");
INSERT INTO `tags`(Name, Description) VALUES("A3", "Assess and give feedback to learners.");
INSERT INTO `tags`(Name, Description) VALUES("A4", "Develop effective learning environments and approaches to student support and guidance.");
INSERT INTO `tags`(Name, Description) VALUES("A5", "Engage in continuing professional development in subjects/disciples and their pedagogy, incorporating research, scholarship and the evaluation of professional practices.");
INSERT INTO `tags`(Name, Description) VALUES("D3.7", "Supporting colleagues.");
INSERT INTO `tags`(Name, Description) VALUES("K1", "The subject material");
INSERT INTO `tags`(Name, Description) VALUES("K2", "Appropriate methods for reaching learning and assessing in the subject area and at the level of the academic programme.");
INSERT INTO `tags`(Name, Description) VALUES("K3", "How students learn, both generally and within their subject/disciplinary area(s)");
INSERT INTO `tags`(Name, Description) VALUES("K4", "The use and value of appropriate learning technologies.");
INSERT INTO `tags`(Name, Description) VALUES("K5", "Methods for evaluating the effectiveness of teaching.");
INSERT INTO `tags`(Name, Description) VALUES("K6", "The implications of quality assurance and quality enhancement for academic and professional practice with a particular focus on teaching.");
INSERT INTO `tags`(Name, Description) VALUES("V1", "Respect individual learners and diverse learning communities.");
INSERT INTO `tags`(Name, Description) VALUES("V2", "Promote participation in higher education and equality of opportunity for learners.");
INSERT INTO `tags`(Name, Description) VALUES("V3", "Use evidence-informed approaches and the outcomes from research, scholarship and continuing professional development.");
INSERT INTO `tags`(Name, Description) VALUES("V4", "Acknowledge the wider context in which higher education operates recognising the implications for professional practice");

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

CREATE USER 'root2'@'localhost' IDENTIFIED BY 'password';
GRANT SELECT ON clientproject.* TO 'root2'@'localhost';
GRANT INSERT ON clientproject.activities TO 'root2'@'localhost';
GRANT INSERT ON clientproject.gridanswers TO 'root2'@'localhost';
GRANT INSERT ON clientproject.keydetails TO 'root2'@'localhost';
GRANT INSERT ON clientproject.reflections TO 'root2'@'localhost';
GRANT INSERT ON clientproject.taglink TO 'root2'@'localhost';
GRANT INSERT ON clientproject.todo TO 'root2'@'localhost';
GRANT INSERT ON clientproject.user TO 'root2'@'localhost';
GRANT INSERT ON clientproject.wordcloud TO 'root2'@'localhost';
